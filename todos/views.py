from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm

# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "all_todo_lists": lists,
    }

    return render(request, "todos/todo_lists.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list,
    }

    return render(request, "todos/todo_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":

        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()

    context = {
        "form": form
    }
    return render(request, "todos/todo_create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = ListForm(instance=list)

    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "todos/todo_edit.html", context)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)

    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    context = {
        "list_object": list,
    }
    return render(request, "todos/todo_delete.html", context)


def todo_item_create(request):
    if request.method == "POST":

        form = ItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = ItemForm()

    context = {
        "form": form
    }
    return render(request, "todos/todo_create_item.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=item)

    context = {
        "item_object": item,
        "form": form,
    }
    return render(request, "todos/todo_edit_item.html", context)
